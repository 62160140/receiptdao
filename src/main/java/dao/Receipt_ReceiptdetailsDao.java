/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.*;

/**
 *
 * @author werapan
 */
public class Receipt_ReceiptdetailsDao implements DaoInterface<Receipt> {

    @Override
    public int add(Receipt object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO receipt (customer_id,user_id,total ) VALUES (?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getCustomer().getId());
            stmt.setInt(2, object.getSeller().getId());
            stmt.setDouble(3, object.getTotal());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
                object.setId(id);
            }
            
            
            for (ReceiptDetail rD : object.getReceiptDetail()) {
                String sqlDetail = "INSERT INTO receipt_detail (receipt_id,product_id,price,amount) VALUES (?,?,?,?)";
                PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
                stmtDetail.setInt(1, rD.getReceipt().getId());
                stmtDetail.setInt(2, rD.getProduct().getId());
                stmtDetail.setDouble(3, rD.getPrice());
                stmtDetail.setInt(4, rD.getAmount());
                int rowDetail = stmtDetail.executeUpdate();
                ResultSet resultDetail = stmtDetail.getGeneratedKeys();
                if (resultDetail.next()) {
                    id = resultDetail.getInt(1);
                    rD.setId(id);
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error to create Receipt");
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT r.id as id,\n"
                    + "    created,\n"
                    + "    customer_id,\n"
                    + "    user_id,\n"
                    + "    u.name as user_name,\n"
                    + "    u.tel as user_tel,\n"
                    + "    c.name as customer_name,\n"
                    + "    c.tel as customer_tel,"
                    + "    total\n"
                    + "FROM customer c,user u,receipt r\n"
                    + "WHERE r.customer_id = c.id AND r.user_id = u.id";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
//                Date created = result.getDate("created"); getMaidai bab nee 
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("created"));
                int customer_id = result.getInt("customer_id");
                int seller_id = result.getInt("user_id");
                String seller_name = result.getString("user_name");
                String seller_tel = result.getString("user_tel");
                String customer_name = result.getString("customer_name");
                String customer_tel = result.getString("customer_tel");
                double total = result.getDouble("total");
                Receipt rc = new Receipt(id, created, new Seller(seller_id, seller_name, seller_tel),
                        new Customer(customer_id, customer_name, customer_tel));
                list.add(rc);
            }
        } catch (SQLException ex) {
            System.out.println("Select all Error!" + ex);
        } catch (ParseException ex) {
            System.out.println("Date parse error!");
        }
        db.close();
        return list;
    }

    @Override
    public Receipt get(int id) {
        Receipt rc = null;
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT r.id as id,\n"
                    + "    created,\n"
                    + "    customer_id,\n"
                    + "    user_id,\n"
                    + "    u.name as user_name,\n"
                    + "    u.tel as user_tel,\n"
                    + "    c.name as customer_name,\n"
                    + "    c.tel as customer_tel,"
                    + "total\n"
                    + "FROM customer c,user u,receipt r\n"
                    + "WHERE r.id = ? AND r.customer_id = c.id AND r.user_id = u.id";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                int rid = result.getInt("id");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("created"));
                int customer_id = result.getInt("customer_id");
                int seller_id = result.getInt("user_id");
                String seller_name = result.getString("user_name");
                String seller_tel = result.getString("user_tel");
                String customer_name = result.getString("customer_name");
                String customer_tel = result.getString("customer_tel");
                double total = result.getDouble("total");
                rc = new Receipt(rid, created, new Seller(seller_id, seller_name, seller_tel),
                        new Customer(customer_id, customer_name, customer_tel));

            }
            //Detail
            getDetails(conn, id, rc);
            return rc;
        } catch (SQLException ex) {
            System.out.println("Error !" + ex.getMessage());
        } catch (ParseException ex) {
            Logger.getLogger(Receipt_ReceiptdetailsDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void getDetails(Connection conn, int id, Receipt rc) throws SQLException {
        String sqlDetail = "SELECT rd.id as id,\n"
                + "       receipt_id,\n"
                + "       product_id,\n"
                + "       p.name as product_name,\n"
                + "       rd.price as price,\n"
                + "       p.price as product_price,\n"
                + "       amount\n"
                + "  FROM receipt_detail rd,product p\n"
                + "  WHERE receipt_id = ? AND rd.product_id = p.id;";
        PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
        stmtDetail.setInt(1, id);
        ResultSet resultDetail = stmtDetail.executeQuery();
        while (resultDetail.next()) {
            int rcid = resultDetail.getInt("id");
            int productId = resultDetail.getInt("product_id");
            String productName = resultDetail.getString("product_name");
            double productprice = resultDetail.getDouble("product_price");
            double price = resultDetail.getDouble("price");
            int amount = resultDetail.getInt("amount");
            Product product = new Product(productId, productName, productprice);
            rc.addReceiptDetail(rcid, product, amount, price);
        }
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "DELETE FROM receipt WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error");
        }

        db.close();
        return row;
    }

    @Override
    public int update(Receipt object) {
        
        return 0;
    }

    public static void main(String[] args) {
        Product p1 = new Product(18, "Oh Leingasdasd", 20);
        Product p2 = new Product(19, "asdasd", 2);
        Seller seller = new Seller(1, "Worawit Werapan", "08888888");
        Customer cus = new Customer(2, "BESTNEW", "0888888");
        Receipt receipt = new Receipt(seller, cus);
        receipt.addReceiptDetail(p1, 9);
        receipt.addReceiptDetail(p2, 10);
        System.out.println("---------------");
        System.out.println(receipt);
        System.out.println("---------------");
        Receipt_ReceiptdetailsDao dao = new Receipt_ReceiptdetailsDao();
        System.out.println("id = " + dao.add(receipt));
        System.out.println("Receipt after add" + receipt);
        System.out.println("Get all" + dao.getAll());

        Receipt newReceipt = dao.get(receipt.getId());
        System.out.println("New Receipt: " + newReceipt);
    }
}
